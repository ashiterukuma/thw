$(function () {
    //Responsiv Tabs
    RESPONSIVEUI.responsiveTabs();

    ///change to dropdown in menu
    $(".bootsnav .menu-item-has-children > ul").removeClass("sub-menu");
    $(".bootsnav .menu-item-has-children > ul").addClass("dropdown-menu");   

    $(".bootsnav .menu-item-594  > ul").addClass("brandsid");   
    $(".bootsnav .menu-item-449  > ul").addClass("spnsid");   
    $(".bootsnav .menu-item-436  > ul").addClass("abtid");   

    $(".bootsnav #navbar-menu ul.mbitemid >li").addClass("dropdown megamenu-fw");   
    $(".bootsnav #navbar-menu ul li.first-menu-item").addClass("dropdown megamenu-fw");   

    $(".bootsnav .menu-item-has-children > ul.dropdown-menu" ).append( "<div class=\"arrow\"></div> " ); 
    $(".bootsnav #navbar-menu ul li.megamenu-fw > a").addClass( "dropdown-toggle" ); 
    $(".bootsnav #navbar-menu ul li.megamenu-fw > a").attr( "data-toggle", "dropdown" );      

    //FAQ
    $('.faq .question').click(function() {     
        var $question = $(this);  
        var $answer = $question.next('.answer');
        // hide other answer not current one
        $('.faq .answer').not($answer).hide('fast');                             
        // show current one
        $answer.show('slow');
    });

    $('#home-slider').nivoSlider({
    		controlNav: false,
    		directionNav: true,
    		animSpeed: 2000,
	        pauseTime: 8000		
    	});
	$('#brands-slider').anythingSlider({
		autoPlay: true,
		buildArrows: false,
		buildNavigation: true,
		hashTags: false,
		delay: 5000
	});

    $('.paginate a').each(function () {
        var $this = $(this);
        var html = $this.html();
        if (html == '&lt;') {
            $this.html('<img src="/images/site/btn-prev.png" alt="Previous"/>');
        } else if (html == '&gt;') {
            $this.html('<img src="/images/site/btn-next.png" alt="Next"/>');
        } else if (html == '&lt;&lt;') {
            $this.html('<img src="/images/site/btn-first.png" alt="First"/>');
        } else if (html == '&gt;&gt;') {
            $this.html('<img src="/images/site/btn-last.png" alt="Last"/>');
        }
    });

    $('.back-top').click(function (e) {
        e.preventDefault();
        $('html,body').animate({scrollTop: 0}, 'slow');
    });

    $('#top-links a, .product-image-lg a, .grid-item-image').mouseenter(function () {
        var $this = $(this);
        $this.animate({opacity: 0.55}, 'fast');
    }).mouseleave(function () {
        var $this = $(this);
        $this.animate({opacity: 1}, 'fast');
    })


    $('#sidebar .sidenav .cur').parents('li:eq(0)').addClass('selected');

    var $sidenav = $('#sidebar .sidenav');
    var $selected = $('.selected', $sidenav);
    if ($selected.length > 0) {
        $selected.parent().addClass('selected');
        $selected.siblings('ul').show();
        var $ul = $selected.parents('ul:eq(0)');
        $ul.siblings('a').addClass('selected');
        $ul.show();
        while (!$ul.hasClass('sidenav')) {
            $ul = $ul.parents('ul:eq(0)');
            $ul.siblings('a').addClass('selected');
            $ul.show();
        }
    }

    $('table tr:even').addClass('even');


    var $search_text = $('#search-text');
    if ($search_text.val() == '') {
        $search_text.val('Products Search');
    }
    $search_text.focus(function (e) {
        var $this = $(this);
        if ($this.val() == 'Products Search') {
            $this.val('');
        } else {
            $this.select();
        }
    }).blur(function (e) {
        var $this = $(this);
        if ($this.val() == '') {
            $this.val('Products Search');
        }
    });
    var $search_form = $('#search-form');
    $search_form.submit(function (e) {
        if ($search_text.val() == '' || $search_text.val() == 'Products Search') {
            e.preventDefault();
            return false;
        }
        return true;
    });


    var $newsletter_text = $('#newsletter-text');
    if ($newsletter_text.val() == '') {
        $newsletter_text.val('*Email Address');
    }
    $newsletter_text.focus(function (e) {
        var $this = $(this);
        if ($this.val() == '*Email Address') {
            $this.val('');
        } else {
            $this.select();
        }
    }).blur(function (e) {
        var $this = $(this);
        if ($this.val() == '') {
            $this.val('*Email Address');
        }
    });


    $('.open-overlay').overlay({
        // custom top position
        top: 100,
        // some mask tweaks suitable for facebox-looking dialogs
        mask: {
            // you might also consider a "transparent" color for the mask
            color: '#fff',
            // load mask a little faster
            loadSpeed: 200,
            // very transparent
            opacity: 0.5
        },
        // disable this for modal dialog-type of overlays
        closeOnClick: false,
        closeOnEsc: true,
        load: true
    });

    $('#header .menu .products > a').addClass('nolink');
    $('#breadcrumbs a').each(function () {
        var $this = $(this);
        if ($this.html() == 'Hydraulic Products') {
            $this.addClass('nolink');
        }
    });

    $('a.nolink').on('click', function (e) {
        e.preventDefault();
    });


    var $resellers = $('body.locate-a-reseller #content table.reseller');
    if ($resellers.length > 0) {
        $('tr', $resellers).on('click', function (e) {
            var $this = $(this);
            var reseller = escape($.trim($('td:eq(0)', $this).html()));
            window.location = 'mailto:info@thwsales.com.au?subject=Contact%20information%20for%20' + reseller + '&body=To%20whom%20it%20may%20concern%3A%0A%0AContact%20Name%3A%0ACompany%20Name%3A%0AContact%20Details%3A%0A%0AI%20would%20like%20to%20get%20in%20contact%20with%20' + reseller + '%20to%20request%20more%20information%20on%20%5BInsert%20Brand%2FProduct%20here%5D.%0ACan%20you%20please%20send%20me%20contact%20information%3F%0A%0AThank%20you%0A';
        });
    }


    //Form validation
    $('#catalogue-form input[name="name"]').focusout(function() {
        var $this = $(this);
        $this.val($.trim($this.val()));
        if($this.val() == '') {                     
            $this.addClass('invalid').removeClass('valid');
            $this.parent().addClass('invalid').removeClass('valid');
            $this.siblings('span').html('Please enter your full name');
        } else {
            $this.addClass('valid').removeClass('invalid');
            $this.parent().addClass('valid').removeClass('invalid');
            $this.siblings('span').html('Full Name');           
        }
    });
    
    $('#catalogue-form input[name="position_title"]').focusout(function() {
        var $this = $(this);
        $this.val($.trim($this.val()));
        if($this.val() == '') {                     
            $this.addClass('invalid').removeClass('valid');
            $this.parent().addClass('invalid').removeClass('valid');
            $this.siblings('span').html('Please enter your position title');
        } else {
            $this.addClass('valid').removeClass('invalid');
            $this.parent().addClass('valid').removeClass('invalid');
            $this.siblings('span').html('Position Title');
        }
    });
    
    $('#catalogue-form input[name="company_name"]').focusout(function() {
        var $this = $(this);
        $this.val($.trim($this.val()));
        if($this.val() == '') {                     
            $this.addClass('invalid').removeClass('valid');
            $this.parent().addClass('invalid').removeClass('valid');
            $this.siblings('span').html('Please enter your company name');
        } else {
            $this.addClass('valid').removeClass('invalid');
            $this.parent().addClass('valid').removeClass('invalid');
            $this.siblings('span').html('Company Name');
        }
    });
    
    $('#catalogue-form input[name="email"]').focusout(function() {
        var $this = $(this);
        $this.val($.trim($this.val()));
        if($this.val() == '') {                     
            $this.addClass('invalid').removeClass('valid');
            $this.parent().addClass('invalid').removeClass('valid');
            $this.siblings('span').html('Please enter your email');
        } else if(!/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test($this.val())) {
            $this.addClass('invalid').removeClass('valid');
            $this.parent().addClass('invalid').removeClass('valid');
            $this.siblings('span').html('Invalid email');
        } else {
            $this.addClass('valid').removeClass('invalid');
            $this.parent().addClass('valid').removeClass('invalid');
            $this.siblings('span').html('Email Address');
        }
    });
    
    $('#catalogue-form input[name="street1"]').focusout(function() {
        var $this = $(this);
        $this.val($.trim($this.val()));
        if($this.val() == '') {                     
            $this.addClass('invalid').removeClass('valid');
            $this.parent().addClass('invalid').removeClass('valid');
            $this.siblings('span').html('Please enter your address');
        } else {
            $this.addClass('valid').removeClass('invalid');
            $this.parent().addClass('valid').removeClass('invalid');
            $this.siblings('span').html('Address');         
        }
    });
    
    $('#catalogue-form input[name="suburb"]').focusout(function() {
        var $this = $(this);
        $this.val($.trim($this.val()));
        if($this.val() == '') {                     
            $this.addClass('invalid').removeClass('valid');
            $this.parent().addClass('invalid').removeClass('valid');
            $this.siblings('span').html('Please enter your suburb');
        } else {
            $this.addClass('valid').removeClass('invalid');
            $this.parent().addClass('valid').removeClass('invalid');
            $this.siblings('span').html('Suburb');          
        }
    });
    
    $('#catalogue-form select[name="state"]').focusout(function() {
        var $this = $(this);
        $this.val($.trim($this.val()));
        if($this.val() == '') {                     
            $this.addClass('invalid').removeClass('valid');
            $this.parent().addClass('invalid').removeClass('valid');
            $this.siblings('span').html('Please select your state');
        } else {
            $this.addClass('valid').removeClass('invalid');
            $this.parent().addClass('valid').removeClass('invalid');
            $this.siblings('span').html('State');           
        }
    });
    
    $('#catalogue-form input[name="postalcode"]').focusout(function() {
        var $this = $(this);
        $this.val($.trim($this.val()));
        if($this.val() == '') {                     
            $this.addClass('invalid').removeClass('valid');
            $this.parent().addClass('invalid').removeClass('valid');
            $this.siblings('span').html('Please enter your postcode');
        } else {
            $this.addClass('valid').removeClass('invalid');
            $this.parent().addClass('valid').removeClass('invalid');
            $this.siblings('span').html('Postcode');            
        }
    });
    
    $('#catalogue-form input[name="country"]').focusout(function() {
        var $this = $(this);
        $this.val($.trim($this.val()));
        if($this.val() == '') {                     
            $this.addClass('invalid').removeClass('valid');
            $this.parent().addClass('invalid').removeClass('valid');
            $this.siblings('span').html('Please enter your country');
        } else {
            $this.addClass('valid').removeClass('invalid');
            $this.parent().addClass('valid').removeClass('invalid');
            $this.siblings('span').html('Country');         
        }
    });
    
    $('#catalogue-form input[name="phone1"]').focusout(function() {
        var $this = $(this);
        $this.val($.trim($this.val()));
        if($this.val() == '') {                     
            $this.addClass('invalid').removeClass('valid');
            $this.parent().addClass('invalid').removeClass('valid');
            $this.siblings('span').html('Please enter your phone');
        } else {
            $this.addClass('valid').removeClass('invalid');
            $this.parent().addClass('valid').removeClass('invalid');
            $this.siblings('span').html('Phone');           
        }
    });

    $('#catalogue-form textarea[name="enquiry"]').focusout(function() {
        var $this = $(this);
        $this.val($.trim($this.val()));
        if($this.val() == '') {                     
            $this.addClass('invalid').removeClass('valid');
            $this.parent().addClass('invalid').removeClass('valid');
            $this.siblings('span').html('Please enter your enquiry');
        } else {
            $this.addClass('valid').removeClass('invalid');
            $this.parent().addClass('valid').removeClass('invalid');
            $this.siblings('span').html('Enquiry');         
        }
    });
    
    $('#catalogue-form').submit(function(e) {

        var $this = $(this);
        $('input, textarea, label', $this).removeClass('invalid');

        var $form_type = $('input[name="form_type"]', $this);
        var $name = $('input[name="name"]', $this);
        var $position_title = $('input[name="position_title"]', $this);
        var $company_name = $('input[name="company_name"]', $this);
        var $email = $('input[name="email"]', $this);
        var $address = $('input[name="street1"]', $this);
        var $suburb = $('input[name="suburb"]', $this);
        var $state = $('select[name="state"]', $this);
        var $postcode = $('input[name="postalcode"]', $this);
        var $country = $('input[name="country"]', $this);
        var $phone = $('input[name="phone1"]', $this);
        var $more_information = $('textarea[name="more_information"]', $this);
        

        var $products = '';
        $('#f_products input[type="checkbox"]').each(function(){
            if($(this).is(':checked')){
                $products += $(this).val() + ',';
            }
        });

        $form_type.val($.trim($form_type.val()));
        $name.val($.trim($name.val()));
        $position_title.val($.trim($position_title.val()));
        $company_name.val($.trim($company_name.val()));
        $email.val($.trim($email.val()));
        $address.val($.trim($address.val()));
        $suburb.val($.trim($suburb.val()));
        $postcode.val($.trim($postcode.val()));
        $country.val($.trim($country.val()));
        $phone.val($.trim($phone.val()));
        $more_information.val($.trim($more_information.val()));
        

        var err = false;
        if($name.val() == '') {
            err = true;
            $name.addClass('invalid').removeClass('valid');
            $name.parent().addClass('invalid').removeClass('valid');
            $name.siblings('span').html('Please enter your full name');
        } else {
            $name.addClass('valid').removeClass('invalid');
            $name.parent().addClass('valid').removeClass('invalid');
            $name.siblings('span').html('Full Name');           
        }
        if($position_title.val() == '') {
            err = true;
            $position_title.addClass('invalid').removeClass('valid');
            $position_title.parent().addClass('invalid').removeClass('valid');
            $position_title.siblings('span').html('Please enter your position title');
        } else {
            $position_title.addClass('valid').removeClass('invalid');
            $position_title.parent().addClass('valid').removeClass('invalid');
            $position_title.siblings('span').html('Position Title');            
        }
        if($company_name.val() == '') {
            err = true;
            $company_name.addClass('invalid').removeClass('valid');
            $company_name.parent().addClass('invalid').removeClass('valid');
            $company_name.siblings('span').html('Please enter your company name');
        } else {
            $company_name.addClass('valid').removeClass('invalid');
            $company_name.parent().addClass('valid').removeClass('invalid');
            $company_name.siblings('span').html('Company Name');            
        }
        if($email.val() == '') {
            err = true;
            $email.addClass('invalid').removeClass('valid');
            $email.parent().addClass('invalid').removeClass('valid');
            $email.siblings('span').html('Please enter your email');
        } else if(!/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test($email.val())) {
            err = true;
            $email.addClass('invalid').removeClass('valid');
            $email.parent().addClass('invalid').removeClass('valid');
            $email.siblings('span').html('Invalid email');
        } else {
            $email.addClass('valid').removeClass('invalid');
            $email.parent().addClass('valid').removeClass('invalid');
            $email.siblings('span').html('Email Address');          
        }
        if($address.val() == '') {
            err = true;
            $address.addClass('invalid').removeClass('valid');
            $address.parent().addClass('invalid').removeClass('valid');
            $address.siblings('span').html('Please enter your address');
        } else {
            $address.addClass('valid').removeClass('invalid');
            $address.parent().addClass('valid').removeClass('invalid');
            $address.siblings('span').html('Address');          
        }
        if($suburb.val() == '') {
            err = true;
            $suburb.addClass('invalid').removeClass('valid');
            $suburb.parent().addClass('invalid').removeClass('valid');
            $suburb.siblings('span').html('Please enter your suburb');
        } else {
            $suburb.addClass('valid').removeClass('invalid');
            $suburb.parent().addClass('valid').removeClass('invalid');
            $suburb.siblings('span').html('Suburb');            
        }
        if($state.val() == '') {                        
            $state.addClass('invalid').removeClass('valid');
            $state.parent().addClass('invalid').removeClass('valid');
            $state.siblings('span').html('Please select your state');
        } else {
            $state.addClass('valid').removeClass('invalid');
            $state.parent().addClass('valid').removeClass('invalid');
            $state.siblings('span').html('State');          
        }
        if($postcode.val() == '') {                     
            $postcode.addClass('invalid').removeClass('valid');
            $postcode.parent().addClass('invalid').removeClass('valid');
            $postcode.siblings('span').html('Please enter your postcode');
        } else {
            $postcode.addClass('valid').removeClass('invalid');
            $postcode.parent().addClass('valid').removeClass('invalid');
            $postcode.siblings('span').html('Postcode');            
        }
        if($country.val() == '') {                      
            $country.addClass('invalid').removeClass('valid');
            $country.parent().addClass('invalid').removeClass('valid');
            $country.siblings('span').html('Please enter your country');
        } else {
            $country.addClass('valid').removeClass('invalid');
            $country.parent().addClass('valid').removeClass('invalid');
            $country.siblings('span').html('Country');          
        }
        if($phone.val() == '') {                        
            $phone.addClass('invalid').removeClass('valid');
            $phone.parent().addClass('invalid').removeClass('valid');
            $phone.siblings('span').html('Please enter your phone');
        } else {
            $phone.addClass('valid').removeClass('invalid');
            $phone.parent().addClass('valid').removeClass('invalid');
            $phone.siblings('span').html('Phone');          
        }
        
         
        if(!err) {           
            //Serialize Form Data Depend on Form Type
            var form_data = [];
            var form_data = {
                "action" : "sendmail",
                "form_type" : $form_type.val(),
                "f_name": $name.val(),
                "f_position": $position_title.val(),
                "f_company": $company_name.val(),
                "f_email": $email.val(),
                "f_address": $address.val(),
                "f_suburb": $suburb.val(),
                "f_state": $state.val(),
                "f_postcode": $postcode.val(),
                "f_country": $country.val(),
                "f_phone": $phone.val(),
                "f_more_information": $more_information.val(),
                "f_product": $products
            };
            
            $.ajax({
                type: "POST",
                url: "/wp-admin/admin-ajax.php",
                data: form_data,
                success: function(response){
                    $('#thank_message').slideDown('slow');
                    $('#processing').hide();
                }
            });
            
        }
        return false;
    }); 


});