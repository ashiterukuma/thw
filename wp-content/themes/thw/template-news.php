<?php /* Template Name: News */ ?>

<?php get_header(); ?>

<div id="content" role="main" class="clearfix news-summary">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="breadcrumbs">
                    <?php
                    if (function_exists('bcn_display')) {
                        bcn_display();
                    }
                    ?>
                </div>
            </div> <!-- end of col12 -->

            <?php get_sidebar(); ?>

            <?php //if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <div id="main" class="">
                    <h1>
                        <?php the_title(); ?>
                        <a href="<?php echo WP_HOME; ?>/feed">
                            <img src="<?php echo ASSET_URL; ?>images/rss.png">
                        </a>
                    </h1>
                    <?php
                        $args = array(
                            'post_type' => 'post',
                            'posts_per_page' => 5,
                            'paged' => (get_query_var('paged')) ? get_query_var('paged') : 1
                        );        
                        $results = new WP_Query($args);        
                        $max_num_pages = $results->max_num_pages;
                        $paged = $args['paged'];
                        //get all results to items
                        $items = $results->posts;
                        if ($items != null) :
                            //get the first item as featured - only on first page
                            if ($args['paged'] == 1) :
                                $item = $items[0];
                                $permalink = get_permalink($item->ID);
                                $title = $item->post_title;
                                $excerpt = $item->post_excerpt;
                                ?>
                                <div class="feature clearfix">
                                    <div class="wrap">
                                        <em><?php echo mysql2date('d F, Y', $item->post_date); ?></em>
                                        <h2><a href="<?php echo $permalink; ?>"><?php echo $title; ?></a></h2>
                                        <?php echo $excerpt; ?>
                                        <a href="<?php echo $permalink; ?>" class="orange readmore">READ MORE</a>
                                    </div>
                                </div>
                                <h2 class="other">Other News</h2>
                                <?php
                                array_shift($items);
                            endif;
                    ?>
                        <ul>
                            <?php
                                foreach ($items as $item) :
                                    $permalink = get_permalink($item->ID);
                                    $title = $item->post_title;
                                    $excerpt = $item->post_excerpt;
                                    $m = mysql2date('M', $item->post_date);
                                    $d = mysql2date('d', $item->post_date);
                            ?>
                            <li>

                                <h3><?php echo $m; ?> <br><span><?php echo $d; ?></span></h3>
                                <h2><a href="<?php echo $permalink; ?>"><?php echo $title; ?></a></h2>
                                <?php echo $excerpt; ?>

                                <a href="<?php echo $permalink; ?>">Read More &gt;</a>
                            </li>
                            <div class="hr"></div>
                            <?php endforeach; ?>
                        </ul>
                        <?php endif; ?>
                        <div id="pagination" class="margintop20 paginate margin-bottom-20">
                            <?php dd_pagination($max_num_pages, $paged); ?>
                        </div>
                </div>    
            </div> <!-- end of col9 -->
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div>
<?php get_footer(); ?>

