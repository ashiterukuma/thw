<?php get_header(); ?>

<div id="content" role="main" class="clearfix container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="home-slider-holder">
                <div id="home-slider-wrap">
                    <div id="home-slider" class="nivoSlider">
                        <?php $sliders = get_field('slider',PAGE_HOME); ?>
                        <?php foreach ($sliders as $slider): ?>
                            <img src="<?php echo $slider['url']; ?>" alt="" title="#home-slide-<?php echo $slider['ID']; ?>"/>
                        <?php endforeach ?>
                    </div>
                    
                    <?php foreach ($sliders as $slider): ?>
                    <div id="home-slide-<?php echo $slider['ID']; ?>" class="nivo-html-caption">
                        <div>
                            <h1><?php echo $slider['title']; ?></h1>
                            <p><?php echo $slider['caption']; ?></p>
                            <a class="orange readmore" href="<?php echo $slider['description']; ?>">READ MORE</a>
                        </div>
                    </div>      
                    <?php endforeach ?>

                    <!-- <div class="slider-border slider-border-top"></div>
                    <div class="slider-border slider-border-bottom"></div>
                    <div class="slider-border slider-border-left"></div>
                    <div class="slider-border slider-border-right"></div> -->
                </div>
            </div>
        </div> <!-- end of col12 -->

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul id="top-links">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="boxwrap shadows6">
                            <li class="request-a-catalogue ">
                                <a href="<?php echo get_permalink(PAGE_NEW_ENQUIRY); ?>">New Account <strong>Enquiries</strong></a>
                            </li>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="boxwrap shadows6 ">
                        <li class="locate-a-reseller"><a href="<?php echo get_permalink(PAGE_RESELLER); ?>">Locate a <strong>Reseller/Repairer</strong></a></li>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="boxwrap shadows6">
                        <li class="contact-us"><a href="<?php echo get_permalink(PAGE_CONTACT); ?>">Contact <strong>Us</strong></a></li>
                        </div>
                    </div>
                </div>
            </ul>        
            <div class="clearhr"></div>
        </div> <!-- end of col12 -->

        <?php $homedata = get_post( PAGE_HOME ); ?>
        <?php $homeimg_url = wp_get_attachment_url(get_post_thumbnail_id($homedata->ID), 'full'); ?>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="wysiwyg">
                <table cellpadding="10" cellspacing="10" width="90%">
                    <tbody>
                        <tr class="even">
                            <td class="hdwidth">
                                <?php echo apply_filters('the_content',$homedata->post_content); ?>
                            </td>
                            <td vailgn="top">
                            <p>
                                <a href="http://www.ozhyd.com.au/hydraulic-products">
                                    <img alt="Clearance" src="<?php echo $homeimg_url; ?>" style="width: 200px; height: 171px; border-width: 0px; border-style: solid;">
                                </a>
                            </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="clearhr"></div>
        </div>    
        
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
            <div class="latest-news">
                <h1>Latest News</h1>
                <?php
                    $args = array(
                        'post_type' => 'post', 
                        'posts_per_page' => 2,
                        'order' => 'DESC',
                        'orderby' => 'date',
                        'post_status' => 'publish',
                        );
                    $news = get_posts($args);
                ?>
                <ul>
                    <?php 
                        foreach ($news as $new):
                        $permalink = get_permalink($new->ID);
                        $title = $new->post_title;
                        $excerpt = $new->post_excerpt;
                    ?>
                    <li>
                        <p class="date"><?php echo get_the_date('d F Y' , $new->ID); ?></p>
                        <h3><?php echo $title; ?></h3>
                        <p></p>
                        <a href="<?php echo $permalink; ?>" class="orange readmore">READ MORE</a>
                    </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <?php 
                $brandsliders = get_field('brands_we_sell',PAGE_HOME); 
                $brandcount = 1; 
                $a = 0;
            ?>
            <div class="brands-we-sell">
                <h1>BRANDS WE SELL</h1>
                <div id="brands-slider-wrapper">
                    <ul id="brands-slider">
                        <li>
                        <?php foreach ($brandsliders as $brandslider): ?>
                            <a class="brand" href="/hydraulic-brands/edi" style="background:url(<?php echo $brandslider['url']; ?>) no-repeat center center #fff;"></a>
                            <?php echo ($brandcount%3==0)? '</li><li>':'' ?>
                        <?php $brandcount++; endforeach ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
