<?php get_header();$current = get_queried_object(); ?>

<?php get_header(); ?>
<div id="content" role="main" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="breadcrumbs">
                    <?php if (function_exists('bcn_display')) { bcn_display(); } ?>
                </div>
            </div>
            <?php get_sidebar(); ?>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 pull-right">
	            <?php 
	                $id = $current->taxonomy.'_'.$current->term_id;
	                $category_img = get_field('category_image',$id );
	            ?>
                <div id="main" class="marginbox0">
                    <h1><?php echo $current->name; ?></h1>
                    <div class="wysiwyg clearfix">
                		<div class="detaillogo shadowstyle6">
							<img class="" src="<?php echo $category_img; ?>">
						</div>
                        <?php echo apply_filters('the_content', $current->description); ?>
                    </div>

	                <div class="hr"></div>
                
					<ul class="hdbrand ">
	            		<?php 
	            			$x = 1;if ( have_posts() ) : 
	                    	while ( have_posts() ) : the_post();  

	                    		if ($x == 1) {
	                                echo '  <div class="row">';
	                            } elseif ($x % 3 == 1) {
	                                echo '<div class="row">';
	                            }
								$postimg_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'full');
						?>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                	<div class="box shadowstyle6">
			                	<li class="text-center ">
									<a href="<?php echo get_permalink($post->ID); ?>">
										<?php if ($postimg_url): ?>
										<div class="grid-item-image" style="background: url(<?php echo $postimg_url; ?>) center center no-repeat rgb(255, 255, 255); opacity: 1;"><img class="" src="<?php echo $postimg_url; ?>"></div>
										<?php endif ?>
									</a>	
			                	</li>
							</div>
							<h2><?php echo $post->post_title; ?></h2>
							<?php 
								$termdatas = get_the_terms( $post->ID, PRODUCT_CATEGO); 
								if ($termdatas) {
									foreach ($termdatas as $termdata):
										if ($termdata->parent == 0): 
							?>
							<p class="text-center"><?php echo $termdata->name; ?></p>
							
							<?php endif;endforeach;} ?>
						</div> <!-- end of col4 -->
							
						<?php if ($x % 3 == 0) { ?> 
	                        </div>
	                    <?php } $x++;endwhile; ?>
	                 	
	                 	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                        <div id="pagination" class="margintop20 paginate margin-bottom-20">
	                            <?php dd_pagination(); ?>
	                        </div>
	                    </div>      
	                 	<?php endif; ?>
                	</ul> <!-- end of hdbrand -->
                </div> <!-- end of main -->
            </div> <!-- end of col9 -->
            
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div> <!-- end of content -->

<?php get_footer(); ?>