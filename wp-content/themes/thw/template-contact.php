<?php /* Template Name: New Acc Enquiry */ ?>
<?php get_header(); ?>
<div id="content" role="main" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="breadcrumbs">
                    <?php
                    if (function_exists('bcn_display')) {
                        bcn_display();
                    }
                    ?>
                </div>
            </div>
            <?php get_sidebar(); ?>
            
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 pull-right">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div id="main">
                    <h1>
                        <?php the_title(); ?>
                        <a href="#" onClick="window.print();return false;" class="print">
                            <img src="<?php echo ASSET_URL; ?>images/btn-print.png" />
                            Print
                        </a>
                    </h1>

                    <div class="wysiwyg">
                        <?php echo apply_filters('the_content', $post->post_content); ?>
                    </div>
                </div>
                <?php endwhile;endif; ?>
                
                <div class="row">
                	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<form action="" accept-charset="utf-8" id="catalogue-form" method="post">

							<fieldset id="catalogue">
								<label>
									<span>Full Name</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em> 
									<input type="text" name="name" class="invalid">
								</label>
								<label>
									<span>Position Title</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em> 
									<input type="text" name="position_title">
								</label>
								<label>
									<span>Company Name</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em>
									<input type="text" name="company_name">
								</label>
						
								<label>
									<span>Email Address</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em>
									<input type="text" name="email">
								</label>
								<label>
									<span>Address</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em>
									<input type="text" name="street1">
								</label>
								<label>
									<span>Suburb</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em> 
									<input type="text" name="suburb">
								</label>
								<div class="labelgroup clearfix">
									<label>
										<span>State</span> 
										<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em> 
										<select name="state">
											<option value=""> - </option>
											<option>ACT</option>
											<option>NSW</option>
											<option>NT</option>
											<option>SA</option>
											<option>TAS</option>
											<option>QLD</option>
											<option>VIC</option>
											<option>WA</option>
											<option>N/A</option>
										</select>
									</label>
									<label>
										<span>Postcode</span>
										<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em>
										<input type="text" name="postalcode">
									</label>
								</div>				
								<label>
									<span>Country</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em>
									<input type="text" name="country">
								</label>
								<label>
									<span>Phone</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em>
									<input type="text" name="phone1">
								</label>
								<label>
									<span>ENQUIRY</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em>
									<textarea name="enquiry"></textarea>
									
								</label>
								<input type="hidden"  name="form_type"  value="contact_form" />
								<p><button type="submit" class="orange submit" value="submit">Submit</button></p>

								<p><div id="processing" style="display: none"><p class="load_process">Processing...please wait</p></div></p>
								<p><small>Required fields marked with</small> <em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em></p>
							</fieldset>
						</form>      
                	
                	</div>
                	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                		<div id="map">
	<h2>
		The Hydraulic Warehouse Pty Ltd</h2>
	<br />
	ABN 28 150 310 759
	<p>
		&nbsp;</p>
	<p>
		31 Powers Road<br />
		Seven Hills NSW 2147</p>
	<p>
		P: 1300 307 925<br />
		F: 1300 937 035<br />
		E: <a href="mailto:info@thwsales.com.au">info@thwsales.com.au</a></p>
	<iframe frameborder="0" height="326" marginheight="0" marginwidth="0" scrolling="no" src="http://maps.google.com.au/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=31+Powers+Rd+Seven+HillsNSW2147&amp;aq=&amp;sll=-25.335448,135.745076&amp;sspn=73.07558,135.263672&amp;vpsrc=6&amp;ie=UTF8&amp;hq=&amp;hnear=31+Powers+Rd,+Seven+Hills+New+South+Wales+2147&amp;t=m&amp;ll=-33.766876,150.954466&amp;spn=0.024973,0.030041&amp;z=14&amp;output=embed" width="330"></iframe></div>
<p>
	&nbsp;</p>

                	</div>
                </div>

            </div>
            
        </div>
    </div>
</div>
<?php get_footer(); ?>

