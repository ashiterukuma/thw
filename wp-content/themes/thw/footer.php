</div><!-- #content-container -->


<div id="footer-container">
    <div id="footer">
        <div class="container">
            <div id="footer-top" class="clearfix">
                <ul class="hydraulic-categories">
                    <li class="heading"><h1>Hydraulic Products</h1></li>
                    <?php 
                        wp_nav_menu(array('container' => false,
                            'theme_location' => 'hyd-products',
                            'items_wrap' => '%3$s'
                            )
                        ); 
                    ?>
                </ul> <!-- end of hydraulic-categories -->

                <ul class="hydraulic-brands">
                    <li class="heading"><h1>Hydraulic Brands</h1></li>          
                    <?php 
                        wp_nav_menu(array('container' => false,
                            'theme_location' => 'foot-brands',
                            'items_wrap' => '%3$s'
                            )
                        ); 
                    ?>
                </ul> <!-- end of hydraulic-brands -->


                <ul class="support">
                    <li class="heading"><h1>Support</h1></li>
                    <?php 
                        wp_nav_menu(array('container' => false,
                            'theme_location' => 'support-services',
                            'items_wrap' => '%3$s'
                            )
                        ); 
                    ?>
                </ul> <!-- end of support -->

                <div class="contact">
                    <h1>The Hydraulic Warehouse</h1>
                    <p>
                        Head Office:
                        <br/>31 Powers Road
                        <br/>Seven Hills, NSW 2147
                        <br/><br/>P: 1300 307 925
                        <br/>F: 1300 937 035
                        <br/>E: <a href="/about-thw/contact-the-hydraulic-warehouse">info@thwsales.com.au</a>
                    </p>
                </div> <!-- end of contact -->

                <div class="sign-up">
                    <h1>Sign Up To Our Newsletter</h1>
                    <p>Stay up-to-date with the latest news from THW.</p>
                    <form action="http://questas.us2.list-manage.com/subscribe/post?u=021ec571f6efd8ff6ea3469b2&amp;id=a0cbd9afa9" method="post" name="mc-embedded-subscribe-form" target="_blank">
                        <input class="search-bx" id="newsletter-text" type="text" name="EMAIL" value="*Email Address" />
                        <input class="orange submit" type="submit" value="Submit" name="subscribe" title="subscribe"/>
                    </form>
                </div> <!-- end of sign-up -->
            </div> <!-- end of footer-top -->

            <div id="footer-bottom">
                <a class="back-top" href="#"><img src="<?php echo ASSET_URL; ?>images/arrow-up.png"/>BACK TO TOP</a>
                <ul>
                    <li class="first-child"><a href="<?php echo get_permalink(PAGE_TERM_CONDITION); ?>">TERMS AND CONDITIONS</a></li>
                    <li><a href="<?php echo get_permalink(PAGE_PRIVACY); ?>">PRIVACY</a></li>
                    <li><a href="/sitemap/">SITEMAP</a></li>
                </ul>
                <p>&copy; 2016 The Hydraulic Warehouse. All Rights Reserved.</p>
            </div> <!-- end of footer-bottom -->
        </div> <!-- end of container -->
    </div> <!-- end of footer -->

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-29211881-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();

    </script>
</div> <!-- end of footer-container -->
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>