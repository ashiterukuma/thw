<?php /* Template Name: Hydraulic Brands */ ?>
<?php get_header(); ?>
<div id="content" role="main" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="breadcrumbs">
                    <?php
                    if (function_exists('bcn_display')) {
                        bcn_display();
                    }
                    ?>
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 pull-right">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div id="main">
                    <h1>
                        <?php the_title(); ?>
                    </h1>

                    <div class="wysiwyg">
                        <?php echo apply_filters('the_content', $post->post_content); ?>
                    </div>
                </div>
                <?php endwhile;endif; ?>

                <?php 
					$terms = get_terms( array(
						'taxonomy' => TAXO_HYDRAULIC_BRANDS,
						'hide_empty' => false,
					));
				?>
				<div class="row">
					<ul class="hdbrand">
					<?php 
						foreach ($terms as $brand):
						$id = $brand->taxonomy.'_'.$brand->term_id;
						$category_img = get_field('category_image',$id );
						// $category_img = aq_resize($category_img, 190, 100, true, true, true);
					?>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		                	<div class="box shadowstyle6">
			                	<li class="text-center ">
									<a href="/<?php echo $brand->taxonomy.'/'.$brand->slug; ?>"><img class="" src="<?php echo $category_img; ?>"></a>	
			                	</li>
							</div>
						</div>
	                <?php endforeach; ?>
	                </ul>
                </div>
                <!-- <div class="row">
				<?php 
					foreach ($terms as $brand):
					$id = $brand->taxonomy.'_'.$brand->term_id;
					$category_img = get_field('category_image',$id );
					// $category_img = aq_resize($category_img, 190, 100, true, true, true);
				?>
				
                	<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                		<div class="brandimgwrap">
							<a href="/<?php echo $brand->taxonomy.'/'.$brand->slug; ?>"><img class="img-responsive" src="<?php echo $category_img; ?>"></a>                		
						</div>
                	</div>
                <?php endforeach; ?>
                </div> -->
            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>

