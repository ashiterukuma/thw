<?php get_header(); ?>
<div id="content" role="main" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="breadcrumbs">
                    <?php
                    if (function_exists('bcn_display')) {
                        bcn_display();
                    }
                    ?>
                </div>
            </div>
            <?php get_sidebar(); ?>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 news-detail">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div id="main" class="">
                    <?php 
                        $m = mysql2date('M', $post->post_date);
                        $d = mysql2date('d', $post->post_date);
                    ?>
                    <h3><?php echo $m; ?> <br><span><?php echo $d; ?></span></h3>
                    <h1>
                        <?php the_title(); ?>
                        <a href="#" onClick="window.print();return false;" class="print">
                            <img src="<?php echo ASSET_URL; ?>images/btn-print.png" />
                            Print
                        </a>
                    </h1>

                    <div class="wysiwyg">
                        <?php echo apply_filters('the_content', $post->post_content); ?>
                    </div>
                <?php endwhile;endif; ?>

                <div class="othernews">
                    <h2 class="other">Other News</h2>
                    <?php
                        $args = array(
                            'post_type' => 'post', 
                            'posts_per_page' => 3,
                            'order' => 'DESC',
                            'orderby' => 'date',
                            'post_status' => 'publish',
                            'exclude' => $post->ID,
                            );
                        $othnews = get_posts($args);
                    ?>
                    <ul>
                        <?php foreach ($othnews as $othnew): ?>
                            <li><a href="<?php echo get_permalink($othnew->ID); ?>"><?php echo $othnew->post_title; ?></a></li>
                        <?php endforeach ?>
                    </ul>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>