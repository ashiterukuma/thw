<?php /* Template Name: New Acc Enquiry */ ?>
<?php get_header(); ?>
<div id="content" role="main" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="breadcrumbs">
                    <?php
                    if (function_exists('bcn_display')) {
                        bcn_display();
                    }
                    ?>
                </div>
            </div>
            <?php get_sidebar(); ?>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 pull-right">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div id="main">
                    <h1>
                        <?php the_title(); ?>
                        <a href="#" onClick="window.print();return false;" class="print">
                            <img src="<?php echo ASSET_URL; ?>images/btn-print.png" />
                            Print
                        </a>
                    </h1>

                    <div class="wysiwyg">
                        <?php echo apply_filters('the_content', $post->post_content); ?>
                    </div>
                </div>
                <?php endwhile;endif; ?>
                
                <div class="row">
                	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<form action="" accept-charset="utf-8" id="catalogue-form" method="post">

							<fieldset id="catalogue">
								<label>
									<span>Full Name</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em> 
									<input type="text" name="name" class="invalid">
								</label>
								<label>
									<span>Position Title</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em> 
									<input type="text" name="position_title">
								</label>
								<label>
									<span>Company Name</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em>
									<input type="text" name="company_name">
								</label>
						
								<label>
									<span>Email Address</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em>
									<input type="text" name="email">
								</label>
								<label>
									<span>Address</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em>
									<input type="text" name="street1">
								</label>
								<label>
									<span>Suburb</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em> 
									<input type="text" name="suburb">
								</label>
								<div class="labelgroup clearfix">
									<label>
										<span>State</span> 
										<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em> 
										<select name="state">
											<option value=""> - </option>
											<option>ACT</option>
											<option>NSW</option>
											<option>NT</option>
											<option>SA</option>
											<option>TAS</option>
											<option>QLD</option>
											<option>VIC</option>
											<option>WA</option>
											<option>N/A</option>
										</select>
									</label>
									<label>
										<span>Postcode</span>
										<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em>
										<input type="text" name="postalcode">
									</label>
								</div>				
								<label>
									<span>Country</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em>
									<input type="text" name="country">
								</label>
								<label>
									<span>Phone</span>
									<em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em>
									<input type="text" name="phone1">
								</label>
								<label class="nopad">
									<span>My Product Interest</span>
								</label>
								<div class="checkgroup clearfix" id="f_products">
									<label><input class="checkbox" type="checkbox" name="products[]" value="ACCESSORIES"> ACCESSORIES</label>
									<label><input class="checkbox" type="checkbox" name="products[]" value="BRAKES"> BRAKES</label>
									<label><input class="checkbox" type="checkbox" name="products[]" value="FILTERS"> FILTERS</label>
									<label><input class="checkbox" type="checkbox" name="products[]" value="GEARBOXES"> GEARBOXES</label>
									<label><input class="checkbox" type="checkbox" name="products[]" value="HYDROSTATIC"> HYDROSTATIC</label>
									<label><input class="checkbox" type="checkbox" name="products[]" value="TRANSMISSIONS"> TRANSMISSIONS</label>
									<label><input class="checkbox" type="checkbox" name="products[]" value="INSTRUMENTATION"> INSTRUMENTATION</label>
									<label><input class="checkbox" type="checkbox" name="products[]" value="MOTORS"> MOTORS</label>
									<label><input class="checkbox" type="checkbox" name="products[]" value="POWER"> POWER</label>
									<label><input class="checkbox" type="checkbox" name="products[]" value="STEERING"> STEERING</label>
									<label><input class="checkbox" type="checkbox" name="products[]" value="PUMPS"> PUMPS</label>
									<label><input class="checkbox" type="checkbox" name="products[]" value="VALVES"> VALVES</label>
								</div>
								<label>
									<span>More Information / Comments:</span>
									<textarea name="more_information"></textarea>
								</label>
								<input type="hidden"  name="form_type"  value="newae_form" />
								<p><button type="submit" class="orange submit" value="submit">Submit</button></p>

								<p><div id="processing" style="display: none"><p class="load_process">Processing...please wait</p></div></p>
								<p><small>Required fields marked with</small> <em><img src="<?php echo ASSET_URL; ?>/images/star.png" alt="required"></em></p>
							</fieldset>
						</form>      
                	
                	</div>
                </div>

            </div>
            
        </div>
    </div>
</div>
<?php get_footer(); ?>

