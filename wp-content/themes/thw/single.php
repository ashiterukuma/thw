<?php get_header(); ?>

<div id="content" role="main" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="breadcrumbs">
                    <?php if (function_exists('bcn_display')) { bcn_display(); } ?>
                </div>
            </div>
            <?php get_sidebar(); ?>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 pull-right">
                <div id="main" class="sgbrandcss">
					<div class="product-brief clearfix">
						<?php $postimg_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'full'); ?>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="row">
									<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
										<div class="box shadowstyle6">
						                	<div class="text-center sg-product-img">
												<?php if ($postimg_url): ?>
												<div class="grid-item-image" style="background: url(<?php echo $postimg_url; ?>) center center no-repeat rgb(255, 255, 255); opacity: 1;"><img class="" src="<?php echo $postimg_url; ?>"></div>
												<?php endif ?>
						                	</div>
										</div>
									</div>
									<div class="product-description">
										<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
											<h1><?php echo $post->post_title; ?></h1>
											<div class="wysiwyg">
												<?php echo $post->post_content; ?>	
											</div>
											<?php 
												$extra_fields = get_fields($post->ID,true);
								                
								                $brochure = $extra_fields['brochure'];
								                $tech_spec = $extra_fields['tech_specs'];
								                $feature = $extra_fields['features'];
								                $applications = $extra_fields['applications'];
								                $dimensions = $extra_fields['dimensions'];
								                $other_products = $extra_fields['other_product'];
						                	?>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<?php if($brochure): ?>
												<a href="<?php echo $brochure; ?>" target="_blank" class="download-brochure orange">Download Brochure</a>
											<?php endif; ?>
											<a href="<?php echo get_permalink(PAGE_RESELLER); ?>" class="where-to-buy orange">Where to Buy</a>
											<a href="<?php echo get_permalink(PAGE_NEW_ENQUIRY); ?>" class="send-an-enquiry orange">Send An Enquiry</a>
										</div>
									</div> <!-- end of product-description -->
								</div> <!-- end of row -->
							</div> <!-- end of col12 -->
						</div> <!-- end of row -->
					</div> <!-- end of product brief -->
					
					<div class="extra-buttons">
						<a href="#" class="a2a_dd share"><img src="<?php echo ASSET_URL; ?>images/plus.png"/>Share</a>
						<a href="#" onClick="window.print();return false;" class="print"><img src="<?php echo ASSET_URL; ?>images/btn-print.png" />Print</a>
					</div> <!-- end of extra-button -->
					
					<div class="responsive-tabs">
						<?php if($tech_spec): ?>
							<h2>Tech Specs</h2>
                            <div>
                                <?php echo $tech_spec; ?>
                            </div>
                        <?php endif; ?>
                        <?php if($feature): ?>
                        	<h2>Features</h2>
                            <div>
                                <?php echo $feature; ?>
                            </div>
                        <?php endif; ?>
                        <?php if($applications): ?>
                        	<h2>Applications</h2>
                            <div>
                                <?php echo $applications; ?>
                            </div>
                        <?php endif; ?>
                        <?php if($dimensions): ?>
                        	<h2>Dimensions</h2>
                            <div class="dimensionimg ">
                                <?php echo $dimensions; ?>
                            </div>
                        <?php endif; ?>
					</div>

                        <div class="hr"></div>
                        
                        <?php if($other_products): ?>

                        <div class="other-products">
							<h1>Other Products</h1>
							<div class="row othprodwrap">
								<?php 
									foreach ($other_products as $product) :
									$prodimg_url = wp_get_attachment_url(get_post_thumbnail_id($product->ID), 'full'); 
								?>
									<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
										<div class="box shadowstyle6">
						                	<div class="text-center othborder">
												<a href="<?php echo get_permalink($product->ID); ?>">
													<?php if ($prodimg_url): ?>
													<div class="grid-item-image" style="background: url(<?php echo $prodimg_url; ?>) center center no-repeat rgb(255, 255, 255); opacity: 1;"><img class="" src="<?php echo $prodimg_url; ?>"></div>
													<?php endif ?>
												</a>	
						                	</div>
										</div>
										<h2><a href="<?php echo get_permalink($product->ID); ?>"><?php echo $product->post_title; ?></a></h2>
										<?php 
											$termdatas = get_the_terms( $product->ID, PRODUCT_CATEGO); 
											if ($termdatas) {
												foreach ($termdatas as $termdata):
													if ($termdata->parent == 0): 
										?>
										<p class="text-center"><?php echo $termdata->name; ?></p>
										<?php endif;endforeach;} ?>
									</div>
								<?php endforeach; ?>
								</div> <!-- end of othprowrap -->
							</div> <!-- end of other-product -->
                        <?php endif; ?>
                    <!--/div> end of horizontaltab -->
                </div> <!-- end of main -->
	        </div> <!-- end of col9 -->
	        
    	</div>
    </div>
</div>

<?php get_footer(); ?>
