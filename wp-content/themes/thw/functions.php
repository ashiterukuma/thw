<?php

define('TEMPLATE_URL', get_stylesheet_directory_uri());
define('ASSET_URL', TEMPLATE_URL . '/assets/');
require_once('includes/aq_resizer.php');

define('PRODUCT_TYPE', 'hydraulic-products');
define('PRODUCT_CATEGO', 'product-category');

define('PAGE_ABOUT', 425);
define('PAGE_HOME', 6);
define('PAGE_SUPPORT_SERVICES', 437);
define('PAGE_RESELLER', 447);
define('PAGE_NEW_ENQUIRY', 443);
define('PAGE_TERM_CONDITION', 637);
define('PAGE_PRIVACY', 640);
define('PAGE_CONTACT', 429);

define('TAXO_HYDRAULIC_BRANDS', 'hydraulic-brands');

#######################################################
/* * ********* CSS merge and minify process *********** */
#######################################################

if ($_SERVER['HTTP_HOST'] == 'thw.dd' || strpos($_SERVER['HTTP_HOST'], '192') !== false) {
    require( 'includes/class.magic-min.php' );
    $minified = new Minifier(
            array(
        'echo' => false
            )
    );
    //exclude example
    $css_exclude = array(            
            // TEMPLATEPATH . '/assets/css/bootstrap.min.css', 
            // TEMPLATEPATH . '/assets/css/master.min.css'            
    );
    //order example
    $css_order = array(
            //TEMPLATEPATH . '/assets/css/reset.css',     
    );

    $minified->merge(TEMPLATEPATH . '/assets/css/master.min.css', TEMPLATEPATH . '/assets/css', 'css', $css_exclude, $css_order);
}

function the_template_url() {
    echo TEMPLATE_URL;
}

################################################################################
// Enqueue Scripts
################################################################################

function init_scripts() {
    wp_deregister_script('wp-embed');
    wp_deregister_script('jquery');
    wp_deregister_script('comment-reply');
}

function add_scripts() {
    $js_path = ASSET_URL . 'js';
    $css_path = ASSET_URL . 'css';
    if ($_SERVER['HTTP_HOST'] == 'thw.dd' || strpos($_SERVER['HTTP_HOST'], '192') !== false) {
        $js_libs = array(
            array(
                'name' => 'jquery',
                'src' => $js_path . '/jquery.js',
            ),
            array(
                'name' => 'jquery.tools',
                'src' => $js_path . '/jquery.tools.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => false
            ),

            array(
                'name' => 'bootstrapjs',
                'src' => '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => false
            ),
            array(
                'name' => 'nivo',
                'src' => $js_path . '/jquery.nivo.slider.pack.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'anything',
                'src' => $js_path . '/jquery.anythingslider.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'bootsnav',
                'src' => $js_path . '/bootsnav.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'responsiveTabs',
                'src' => $js_path . '/responsiveTabs.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => false
            ),
            array(
                'name' => 'script',
                'src' => $js_path . '/script.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
        );

        $css_libs = array(
//            array(
//                'name' => 'bootstrap',
//                'src' => $css_path . '/bootstrap.min.css',
//            ),
//            array(
//                'name' => 'font-awesome',
//                'src' => $css_path . '/font-awesome.min.css',
//            ),
            // array(
            //     'name' => 'easy responsive',
            //     'src' => $css_path . '/easy-responsive-tabs.css',
            //     'dep' => null,
            //     'ver' => null,
            //     'media' => 'screen'
            // ),
            array(
                'name' => 'style',
                'src' => TEMPLATE_URL . '/style.css',
            ),
            array(
                'name' => 'print',
                'src' => TEMPLATE_URL . '/print.css',
                'dep' => null,
                'ver' => null,
                'media' => 'print'
            ),
        );
    } else {
        $js_libs = array(
            array(
                'name' => 'jquery',
                'src' => '//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js',
            ),
            array(
                'name' => 'jquery.tools',
                'src' => $js_path . '/jquery.tools.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => false
            ),
            array(
                'name' => 'nivo',
                'src' => $js_path . '/jquery.nivo.slider.pack.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'anything',
                'src' => $js_path . '/jquery.anythingslider.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'script',
                'src' => $js_path . '/script.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
        );

        $css_libs = array(
            array(
                'name' => 'bootstrap',
                'src' => $css_path . '/bootstrap.min.css',
            ),
            array(
                'name' => 'font-awesome',
                'src' => $css_path . '/font-awesome.min.css',
            ),
            array(
                'name' => 'style',
                'src' => TEMPLATE_URL . '/style.css',
            ),
            array(
                'name' => 'print',
                'src' => TEMPLATE_URL . '/print.css',
                'dep' => null,
                'ver' => null,
                'media' => 'print'
            ),
        );
    }
    foreach ($js_libs as $lib) {
        wp_enqueue_script($lib['name'], $lib['src'], $lib['dep'], $lib['ver'], $lib['is_footer']);
    }
    foreach ($css_libs as $lib) {
        wp_enqueue_style($lib['name'], $lib['src'], $lib['dep'], $lib['ver'], $lib['media']);
    }
}

function my_deregister_scripts() {
    global $post_type;

    if (!is_front_page() && !is_home()) {
        
    }
    if (!is_page('contact-us')) { //only include in contact us page
    }
}

function my_deregister_styles() {
    global $post_type;
    if (!is_page('contact-us')) { //only include in contact us page
    }
}

if (!is_admin())
    add_action('init', 'init_scripts', 10);
add_action('wp_enqueue_scripts', 'add_scripts', 10);
add_action('wp_enqueue_scripts', 'my_deregister_scripts', 100);
add_action('wp_enqueue_scripts', 'my_deregister_styles', 100);



################################################################################
// Add theme support
################################################################################

add_theme_support('automatic-feed-links');
add_theme_support('nav-menus');
add_post_type_support('page', 'excerpt');
add_theme_support('post-thumbnails', array('post', 'page', PRODUCT_TYPE));

if (function_exists('register_nav_menus')) {
    register_nav_menus(
            array(
                'main' => 'Main',
                'helpful' => 'Helpful Info',
                'privacy' => 'Privacy Info',
                'about-thw' => 'About THW',
                'support-services' => 'Support and Services',
                'hyd-brands' => 'Hydraulic Brands',
                'foot-brands' => 'Footer Brands',
                'hyd-products' => 'Hydraulic Products',
                'sidehyd-products' => 'SideBar Hydraulic Products'
            )
    );
}

################################################################################
// Add theme sidebars
################################################################################

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => __('Main - Sidebar'),
        'id' => 'main-sidebar-widget-area',
        'description' => 'Widgets in this area will be shown on the right sidebar of default page',
        'before_widget' => '<aside class="widget">',
        'after_widget' => '</aside>',
        'before_title' => '',
        'after_title' => '',
    ));
//    register_sidebar(array(
//        'name' => __('Page - Sidebar'),
//        'id' => 'page-widget-area',
//        'description' => 'Widgets in this area will be shown on the right sidebar of pages',
//        'before_widget' => '<aside class="widget">',
//        'after_widget' => '</aside>',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Newsletter - Sidebar'),
//        'id' => 'footer-newsletter-widget-area',
//        'description' => 'Widgets in this area will be shown on the top of footer',
//        'before_widget' => '<div class="row" id="newsletter-form-container">',
//        'after_widget' => '</div>',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Left - Sidebar'),
//        'id' => 'footer-left-widget-area',
//        'description' => 'Widgets in this area will be shown on the left-hand side of footer',
//        'before_widget' => '',
//        'after_widget' => '',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Middle - Sidebar'),
//        'id' => 'footer-middle-widget-area',
//        'description' => 'Widgets in this area will be shown in the middle of footer',
//        'before_widget' => '',
//        'after_widget' => '',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Right - Sidebar'),
//        'id' => 'footer-right-widget-area',
//        'description' => 'Widgets in this area will be shown on the right-hand side of footer',
//        'before_widget' => '',
//        'after_widget' => '',
//        'before_title' => '',
//        'after_title' => '',
//    ));
}





################################################################################
// Comment formatting
################################################################################

function theme_comments($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    ?>
    <li>
        <article <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
            <header class="comment-author vcard">
                <?php echo get_avatar($comment, $size = '48', $default = '<path_to_url>'); ?>
                <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
                <time><a href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time()) ?></a></time>
                <?php edit_comment_link(__('(Edit)'), '  ', '') ?>
            </header>
            <?php if ($comment->comment_approved == '0') : ?>
                <em><?php _e('Your comment is awaiting moderation.') ?></em>
                <br />
            <?php endif; ?>

            <?php comment_text() ?>

            <nav>
                <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
            </nav>
        </article>
        <!-- </li> is added by wordpress automatically -->
        <?php
    }

    if (!function_exists('dd_pagination')) :

        function dd_pagination($max_num_pages=0, $paged=0) {

            global $wp_query;            
            $big = 999999999; // need an unlikely integer
            echo paginate_links(array(
                'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                'format' => '?paged=%#%',
                'current' => max(1, ($paged > 0) ? $paged : get_query_var('paged')),
                'total' => ($max_num_pages > 0) ? $max_num_pages : $wp_query->max_num_pages,
                'show_all' => false,
                'end_size' => 1,
                'mid_size' => 2,
                'prev_next' => true,
                'prev_text' => __('<img src="/wp-content/themes/thw/assets/images/btn-prev.png">'),
                'next_text' => __('<img src="/wp-content/themes/thw/assets/images/btn-next.png">'),

            ));
        }
    endif;
    //Over Wride post per page



    function product_archive($query) {     
        if($query->is_main_query() && !is_admin()){
            if (is_tax(TAXO_HYDRAULIC_BRANDS)) {
                $query->set('posts_per_page', '6');    
            }
            if (is_tax(PRODUCT_CATEGO)) {
                $query->set('posts_per_page', '9');    
            }
            // if (is_post_type_archive(PRODUCT_TYPE)){
            //      $query->set('posts_per_page', '3');
            // }

            // elseif(is_category(NEWS_CATEGORIES)) {
            //     $query->set('posts_per_page', '5');
            // }elseif(is_search()){
            //   $query->set('posts_per_page', '12');   
            // }else{
            //     $query->set('posts_per_page', '9');
            // }
        }
    }

    add_action('pre_get_posts', 'product_archive');



    /* ======================
     * ajax sendmail function
     * ======================
     */

    function ajax_sendmail_func() {

        GLOBAL $THEME_OPTIONS;


        if ($_POST['form_type'] == 'newae_form') {

            $to = $THEME_OPTIONS['info_email'];
            $from = $_POST['f_email'];

            $name = $_POST['f_name'];
            $position = $_POST['f_position'];
            $company = $_POST['f_company'];
            $phone = $_POST['f_phone'];
            $address = $_POST['f_address'];
            $suburb = $_POST['f_suburb'];
            $postcode = $_POST['f_postcode'];
            $state = $_POST['f_state'];
            $country = $_POST['f_country'];
            $product = rtrim( $_POST['f_product'], ",");

            $more_information = $_POST['f_more_information'];
            
            $subject = "Contact from New Account Enquiries Form";
            
            $personal_info =  " FULL NAME          : ". $name . "<br/>
                                EMAIL ADDRESS      : ". $from ."<br>
                                POSITION TITLE     : ". $position ."<br>
                                COMPANY NAME       : ". $company ."<br>
                                Contact Number     : ". $phone . "<br/>
                                ADDRESS            : ". $address ."<br>
                                SUBURB             : ". $suburb ."<br>
                                POSTCODE           : ". $postcode ."<br>
                                STATE              : ". $state ."<br>
                                COUNTRY            : ". $country ."<br>
                                PRODUCT INTEREST   : ". $product ."<br>
                                MORE INFORMATION / COMMENTS  : " . $more_information ."<br>";

            $body = "Hi! you have new enquiry from " . $name . "<br /><br/> Customer Information <br/><br/>" . $personal_info;
        }elseif ( $_POST['form_type']=='contact_form' ) {

            $to = $THEME_OPTIONS['info_email'];
            $from = $_POST['f_email'];

            $name = $_POST['f_name'];
            $position = $_POST['f_position'];
            $company = $_POST['f_company'];
            $phone = $_POST['f_phone'];
            $address = $_POST['f_address'];
            $suburb = $_POST['f_suburb'];
            $postcode = $_POST['f_postcode'];
            $state = $_POST['f_state'];
            $country = $_POST['f_country'];
            $enquiry = $_POST['f_enquiry'];
            
            $subject = "Enquiry from Contact Form";

            $personal_info =  " FULL NAME          : ". $name . "<br/>
                                EMAIL ADDRESS      : ". $from ."<br>
                                POSITION TITLE     : ". $position ."<br>
                                COMPANY NAME       : ". $company ."<br>
                                Contact Number     : ". $phone . "<br/>
                                ADDRESS            : ". $address ."<br>
                                SUBURB             : ". $suburb ."<br>
                                POSTCODE           : ". $postcode ."<br>
                                STATE              : ". $state ."<br>
                                COUNTRY            : ". $country ."<br>
                                ENQUIRY            : ". $enquiry ."<br>";
            $body = "Hi! you have new enquiry from " . $name . "<br /><br/> Customer Information <br/><br/>" . $personal_info;
        }

        $content .= "<html>\n";
        $content .= "<body style=\"font-family:Verdana, Verdana, Geneva, sans-serif; font-size:12px; color:#666666;\">\n";
        $body = html_entity_decode($body);
        $content .= $body;
        $content .= "\n\n";
        $content .= "</body>\n";
        $content .= "</html>\n";
        $headers .= 'From: The Hydraulic Warehouse Australia <noreply@thehydraulicwarehouse.com' . "\r\n";
        $headers .= 'Reply-To:' . $name . '<' . $from . '>' . "\r\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=UTF-8\r\n";

        if (!$to) {  // use admin mail if there is no reservation form mail
            $to = get_option('admin_email');
        } else {
            if (strpos($to, ',')) { // if it is comman separated, changed to array
                $to = explode(',', str_replace(' ', '', $to));
            }
        }
        
        if (wp_mail($to, $subject, $content, $headers)) {
            $data['status'] = 'yes';
            echo $body;
        } else {
            $data['status'] = 'no';
            echo $body;
        }
        echo json_encode($data);
        die;
}
add_action('wp_ajax_sendmail', 'ajax_sendmail_func');           // for logged in user  
add_action('wp_ajax_nopriv_sendmail', 'ajax_sendmail_func');





// function add_classes_on_li($classes, $item, $args) {
//   $classes[] = 'dropdown megamenu-fw';
//   return $classes;
// }

// add_filter('nav_menu_css_class','add_classes_on_li',1,3);

