<?php
    if (is_page(PAGE_ABOUT) || $post->post_parent == PAGE_ABOUT) {
        $menu_location = "about-thw";
    }
    elseif (is_page(PAGE_SUPPORT_SERVICES) || $post->post_parent == PAGE_SUPPORT_SERVICES) {
        $menu_location = "support-services";
    }elseif(is_tax){
        $current_term = get_queried_object();
        if( is_tax(TAXO_HYDRAULIC_BRANDS ) ){
            $menu_location = 'foot-brands';
        }elseif(is_tax(PRODUCT_CATEGO)){
            $menu_location = 'sidehyd-products';
        }
    }    
                                
?>

<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 pull-left marginbot20">
    <div id="sidebar">
        <ul class="sidenav pagesidenav selected">
            <?php
                wp_nav_menu(
                    array('container' => false,
                        'theme_location' => $menu_location,
                        'link_before' => '<span>',
                        'link_after' => '</span>',
                        'items_wrap' => '%3$s',
                ));
            ?>
        </ul>
    </div>
    <!-- <div id="sidebar">
        <ul class="sidenav">
            <?php 
                wp_nav_menu(array('container' => false,
                    'theme_location' => 'foot-brands',
                    'items_wrap' => '%3$s'
                )); 
            ?>
        </ul>
    </div> -->
</div>



