<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
<title><?php wp_title(''); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
<link rel="shortcut icon" href="<?php echo TEMPLATE_URL ?>/favicon.ico">  
<meta http-equiv="Content-Language" content="en-au" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta name="google-site-verification" content="pTyxcbDyRLZXRrS8RLY7HncoGyQa1M-fN4IIyvS1QJA" />
<?php wp_head(); ?>

</head>
<?php global $THEME_OPTIONS; ?>
<?php $body_classes = join(' ', get_body_class()); ?>
<body class="<?php if (!is_search()) echo $body_classes; ?>">
    <?php // wp_nav_menu( array( 'container' => false, 'container_class' => 'primary-nav', 'menu' => 'Primary', 'link_before' => '<span>','link_after' => '</span>')); ?>   
    <div id="page">
        <div id="header-container">
            <div class="container">
                <div id="header">
                    <div id="header-top" class="clearfix">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                                <div class="mbcenter">
                                <ul>
                                    <li class="first-child"><a href="#" class="a2a_dd"><img src="<?php echo ASSET_URL; ?>images/plus.png"/>SHARE OR BOOKMARK</a></li>
                                    <li><a href="/about-thw/the-hydraulic-warehouse-news/rss" target="_blank"><img src="<?php echo ASSET_URL; ?>images/rss.png"/></a><a href="/about-thw/the-hydraulic-warehouse-news">NEWS</a></li>
                                    <li><a href="/support-and-services/new-account-enquiries">NEW ACCOUNT ENQUIRIES</a></li>
                                </ul>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                                <script type="text/javascript">
                                    a2a_config = {
                                        linkname: document.title,
                                        linkurl: window.location.href,
                                        num_services: 10,
                                        show_title: 1
                                    };
                                </script>
                                
                                <form id="search-form" method="post" action="http://www.thehydraulicwarehouse.com.au/"  >
                                    <div class='hiddenFields'>
                                        <input type="hidden" name="ACT" value="1" />
                                        <input type="hidden" name="RES" value="8" />
                                        <input type="hidden" name="meta" value="s3LGj5V0H9VJE2tCYRqo2iCdB6u0MhUyecfJpZnnV/jcigTkDWmWLgWyayU4qEubip0n42zQq0oG83o+l/yYPcybxGlYasoc6BnKKAwrQA7SZnCPrQXxXZpQeU76Bcb5THp7B5JezWowDr9f/TU3jyiFwzxCPgDr727fkYvKtXx6qpVDkMiwz1ZBNc0HbME8sxYeidjM4AQk0ql8kkXeNeZstKNRgsF7jJpenEt0qvx4WfQi0ciqSwqy1ItAIRSRD1KQLV1yN+LIwuAmIc9Yr4/lKbvTvbX0ljo56bmKP1gN9c6JBY1LZOz26nKC3CC+OjfaNdvkpzZkYt22piThVWby9tYDpxNc6I18r8E+BKSX0oPZig9Ir6dkJ0+SwHIK" />
                                        <input type="hidden" name="site_id" value="1" />
                                        <input type="hidden" name="csrf_token" value="868321944103dea5bfc10c7dea4bfa29b6510829" />
                                    </div>
                                    <input id="search-text" class="search-bx" type="text" name="keywords" value="" />
                                    <input class="search-btn orange" type="submit" value="Go" name="go" title="search"/>
                                </form>
                                <a href="http://websales.questas.com.au:8080/THWweborderentry/Login.htm" class="orange login" target="_blank">Online Ordering Login</a>      
                            </div>
                        </div>
                    </div>
                </div> <!-- end of header -->
            </div> <!-- end of container -->
            <div class="headermd">
            <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <?php 
                                $terms = get_terms( array(
                                    'taxonomy' => PRODUCT_CATEGO,
                                    'hide_empty' => false,
                                    'parent' => 0,
                                ));
                            ?>
                            <nav class="navbar navbar-default bootsnav">
                                <!-- <div class="container">       -->
                                    <!-- Start Header Navigation -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                                            <i class="fa fa-bars"></i>
                                        </button>
                                        <a class="navbar-brand" href="/"><img src="<?php echo $THEME_OPTIONS['logo']; ?>" class="logo" alt="The Hydraulic Warehouse Australia"></a>
                                        <img src="<?php echo ASSET_URL ?>images/home.png" class="homevisible">
                                    </div>
                                        <h1 class="contact">1300 307 925</h1>
                                    <!-- End Header Navigation -->

                                    <!-- Collect the nav links, forms, and other content for toggling -->

                                    <div class="collapse navbar-collapse" id="navbar-menu">
                                        <ul class="ulmargintop40 nav navbar-nav navbar-right mbitemid" data-in="fadeInDown" data-out="fadeOutUp">
                                            <li class="">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">HYDRAULIC PRODUCTS</a>

                                                <ul class="dropdown-menu megamenu-content" role="menu">
                                                    <div class="arrow"></div>    
                                                    <li>
                                                        <div class="row">
                                                            <?php $prdcount = 1; if ($terms): ?>
                                                            <?php foreach ($terms as $prodcate): ?>
                                                            <?php 
                                                                $prodcateid = $prodcate->taxonomy.'_'.$prodcate->term_id;
                                                                $prodcateimg_url = get_field('category_image',$prodcateid);
                                                                $menu_excerpt = get_field('menu_excerpt',$prodcateid); 
                                                            ?>
                                                            <div class="nopadding col-md-3">
                                                                <div class="megamenuwrap">
                                                                    <a class="top " href="/<?php echo $prodcate->taxonomy.'/'.$prodcate->slug; ?>">
                                                                        <div class="text-center menuborder" style="background:url(<?php echo $prodcateimg_url; ?>) center center no-repeat #fff">
                                                                            <img src="<?php echo $prodcateimg_url; ?>" alt="<?php echo $prodcate->name; ?>" />
                                                                        </div>
                                                                        <h3 class="text-center"><?php echo $prodcate->name; ?></h3>
                                                                        <p class="text-center"><?php echo $menu_excerpt; ?></p>
                                                                    </a>
                                                                </div>
                                                            </div><!-- end col-3 -->
                                                            <?php echo ($prdcount%4==0)? '</div><div class="row">':'' ?>
                                                            <?php $prdcount++; endforeach ?>
                                                                <?php endif ?>
                                                        </div><!-- end row -->
                                                    </li>
                                                </ul>
                                            </li>
                                            
                                                <?php
                                                
                                                    wp_nav_menu(array('container' => false,
                                                        'theme_location' => 'hyd-brands',
                                                        'items_wrap' => '%3$s'
                                                            )
                                                    );
                                                    wp_nav_menu(array('container' => false,
                                                        'theme_location' => 'support-services',
                                                        'items_wrap' => '%3$s'
                                                            )
                                                    );
                                                    wp_nav_menu(array('container' => false,
                                                        'theme_location' => 'about-thw',
                                                        'items_wrap' => '%3$s'
                                                            )
                                                    );
                                                ?>  
                                            
                                            
                                        </ul>
                                    </div><!-- /.navbar-collapse -->
                                <!-- </div> --> <!-- end of container -->
                            </nav>    
                        </div> <!-- end of col12 -->
                    </div> <!-- end of row -->
                </div> <!-- end of container -->
            </div> <!-- end of headermd -->
        </div> <!-- end of header-container -->
                



        <div id="content-container">
            