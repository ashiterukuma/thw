<?php get_header(); ?>
<div id="content" role="main" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="breadcrumbs">
                    <?php
                    if (function_exists('bcn_display')) {
                        bcn_display();
                    }
                    ?>
                </div>
            </div>
            <?php get_sidebar(); ?>
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div id="main">
                    <h1>
                        <?php the_title(); ?>
                        <a href="#" onClick="window.print();return false;" class="print">
                            <img src="<?php echo ASSET_URL; ?>images/btn-print.png" />
                            Print
                        </a>
                    </h1>

                    <div class="wysiwyg">
                        <?php echo apply_filters('the_content', $post->post_content); ?>
                    </div>
                </div>
                <?php endwhile;endif; ?>

                <?php $downloads = get_field('downloads',$post->ID); ?>
                <?php if ($downloads): ?>
                    <?php foreach ($downloads as $downloadfile): ?>
                    <?php $fsize = size_format(filesize( get_attached_file( $downloadfile['download_file']['ID'] ) ) , 1); ?>
                        <ul class="downloads">
                            <li class="odd">
                                <a href="<?php echo $downloadfile['download_file']['url']; ?>" target="_blank">
                                    <span class="size"><?php echo $fsize; ?></span><span class="name"><?php echo $downloadfile['download_file']['title']; ?></span>
                                </a>
                            </li>
                        </ul>
                    <?php endforeach; ?>
                <?php endif ?>

                <?php $faqs = get_field('faq',$post->ID); ?>
                <?php if ($faqs): ?>
                    <div class="faq">
                        <?php foreach ($faqs as $faq) : ?>
                        <h2 class="question"><span class=""></span><?php echo $faq['question']; ?></h2>
                        <div class="answer" style="display: none;"><p><?php echo $faq['answer']; ?></p></div>
                        <?php endforeach; ?>
                    </div>
                <?php endif ?>

                <?php $profilepdf = get_field('profile_pdf',$post->ID); ?>
                <?php if ($profilepdf): ?>
                <?php $fsize = size_format(filesize( get_attached_file( $profilepdf['ID'] ) ) , 1); ?>
                    <ul class="downloads">
                        <li class="odd">
                            <a href="<?php echo $profilepdf['url']; ?>" target="_blank">
                                <span class="size"><?php echo $fsize; ?></span><span class="name"><?php echo $profilepdf['title']; ?></span>
                            </a>
                        </li>
                    </ul>
                <?php endif ?>

            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>